# Useful Scripts

### gifenc 
Convert video files to small file size .gifs without sacrificing quality.
Usage
```
chmod +x gifenc
./gifenc input.mov output.gif
```
